export const containerClasses = [
  'container',
  'max-w-6xl',
  'mx-auto',
  'px-4',
  'py-1'
].join(' ')
