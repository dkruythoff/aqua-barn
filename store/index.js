import data from '../assets/data.json'

export const actions = {
  async nuxtServerInit({ commit }) {
    await commit("site/setContent", data);
  }
};
