export const state = () => ({
  content: {}
});

export const mutations = {
  setContent(state, content) {
    state.content = content
  }
};

export const getters = {
  lastUpdate: ({ content: { lastUpdate } }) => new Date(lastUpdate),
  texts: ({ content: { data, byType } }) => data[byType.texts[0]],
  tree: ({ content: { tree, data } }) => Object.entries(tree).reduce((acc, [categorySlug, categoryEntry]) => {
    const subcategories = Object.entries(categoryEntry.subcategories).reduce((acc, [subcategorySlug, subcategoryEntry]) => {
      const products = Object.entries(subcategoryEntry.products).reduce((acc, [productSlug, productEntry]) => {
        acc[productSlug] = { ...productEntry, data: data[productEntry._id] }
        return acc
      }, {})

      acc[subcategorySlug] = { ...subcategoryEntry, products, data: data[subcategoryEntry._id] }
      return acc
    }, {})
    const products = Object.entries(categoryEntry.products).reduce((acc, [productSlug, productEntry]) => {
      acc[productSlug] = { ...productEntry, data: data[productEntry._id] }
      return acc
    }, {})

    acc[categorySlug] = { ...categoryEntry, subcategories, products, data: data[categoryEntry._id] }
    return acc
  }, {}),
  productUrlMap: ({ content: { routes } }) => routes.reduce((acc, route) => ({ ...acc, [route.path]: route.title }), {}),
  productsByCategory: ({ content: { data, productsByCategory } }) => Object.entries(productsByCategory).reduce((acc, [categorySlug, productsObj]) => {
    const products = Object.entries(productsObj).reduce((acc, [productSlug, productEntry]) => ({ ...acc, [productSlug]: { ...productEntry, data: { ...data[productEntry._id], _path: ['/producten', categorySlug, productSlug].join('/') } } }), {})
    return { ...acc, [categorySlug]: products }
  }, {})
}
