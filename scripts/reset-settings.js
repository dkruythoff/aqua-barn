const { join } = require('path')
const { writeFileSync } = require('fs')

const settingsPath = join(__dirname, '..', 'assets', 'content', 'settings.json')
const settings = require(settingsPath)

writeFileSync(settingsPath, JSON.stringify({
  ...settings,
  preview: false,
  release: false
}), 'utf8')
