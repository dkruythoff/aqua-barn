const { resolve, join } = require('path')
const { writeFileSync } = require('fs')

const { getFormattedData } = require('./helpers/get-formatted-data')
const dir = resolve(__dirname, "../assets/content")

const data = getFormattedData(dir)

writeFileSync(join(dir, '../data.json'), JSON.stringify(data, undefined, 2), 'utf8')
