const { resolve, dirname, join } = require('path')
const { readdirSync, existsSync, unlinkSync, statSync, mkdirSync } = require('fs')
const sharp = require('sharp')
const { readdirRecursSync } = require('./helpers/readdir-recurs-sync')

const { screens } = require('./helpers/screen-sizes')

const baseDir = resolve(__dirname, '..', 'static')
const srcDir = join(baseDir, 'uploads')
const dstBase = join(dirname(srcDir), 'res')
const srcFiles = readdirSync(srcDir)
const srcFilesMtime = {}
const imageSizes = { ...screens, xs: '320px' }

Object.entries(imageSizes).forEach(([scrName, scrResPx]) => {
  const scrRes = parseInt(scrResPx.substring(0, scrResPx.length - 2), 10)
  const dstDir = join(dstBase, scrName)

  if (!existsSync(dstDir)) {
    console.log(`Directory '${dstDir}' not found. Creating it.`)
    mkdirSync(dstDir, { recursive: true })
  } else {
    readdirRecursSync(dstDir).forEach((dstFile) => {
      if (!srcFiles.includes(dstFile)) {
        console.log(`Original of '${dstFile}' not found. Unlinking.`)
        unlinkSync(join(dstBase, scrName, dstFile))
      }
    })
  }

  srcFiles.forEach((filePath) => {
    const srcFile = join(srcDir, filePath)
    const dstFile = join(dstDir, filePath)
    const dstExists = existsSync(dstFile)
    const dstMtimeMs = dstExists ? statSync(dstFile) : null
    let processFile = false

    srcFilesMtime[filePath] = srcFilesMtime[filePath] || statSync(srcFile).mtimeMs

    if (!dstExists) {
      console.log(`File '${join(scrName, filePath)}' not found. Creating.`)
      processFile = true
    } else if (dstMtimeMs < srcFilesMtime[filePath]) {
      console.log(`File '${join(scrName, filePath)}' outdated. Recreating.`)
      processFile = true
    }

    if (processFile) {
      sharp(srcFile)
        .resize(scrRes)
        .webp({ lossless: true, force: false })
        .jpeg({ progressive: true, force: false })
        .png({ progressive: true, force: false })
        .toFile(dstFile)
    }
  })
})
