const { readdirSync, statSync } = require('fs')
const { join } = require('path')

function readdirRecursSync(dir, filelist = [], topDir = null) {
  const files = readdirSync(dir)
  topDir = topDir || dir

  files.forEach(function (file) {
    file = join(dir, file)

    if (statSync(file).isDirectory()) {
      filelist = readdirRecursSync(file, filelist, topDir)
    } else {
      filelist.push(file.replace(topDir, '').replace(/^\//, ''))
    }
  })

  return filelist
}

module.exports = { readdirRecursSync }
