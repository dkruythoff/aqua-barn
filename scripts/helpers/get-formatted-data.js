const { resolve, sep } = require('path')
const { readFileSync } = require('fs')
const crypto = require('crypto')

const { readdirRecursSync } = require('./readdir-recurs-sync')
const { screens } = require('./screen-sizes')

function getImageArray(product) {
  const { images } = product
  return Object.entries(images || {}).reduce((acc, [, image]) =>
    ([...acc, ...(image ? [image] : [])]), [])
}

const imageSizes = Object.entries(screens).reduce((acc, [key, sizePx]) => ([...acc, {
  key,
  size: parseInt(sizePx.substring(0, sizePx.length - 2), 10)
}]), [])

const imageBreakpoints = {
  sm: `(max-width: ${screens.sm})`,
  md: `(min-width: ${screens.sm}) and (max-width: ${screens.md})`,
  lg: `(min-width: ${screens.md}) and (max-width: ${screens.lg})`,
  xl: `(min-width: ${screens.lg})`
}

function getPictures(imageArray) {
  return imageArray.reduce((acc, imagePath) => {
    acc[imagePath] = imageSizes.reduce((acc, { key, size }) => ([
      ...acc,
      {
        key,
        size,
        path: imagePath.replace('/uploads', `/res/${key}`),
        mq: imageBreakpoints[key]
      }
    ]), [])
    return acc
  }, {})
}

function getFormattedData(dir) {
  const files = readdirRecursSync(dir)
  const data = {}
  const byType = {}
  const productsByCategory = {}

  files.forEach((filePath) => {
    const content = JSON.parse(readFileSync(resolve(dir, filePath), 'utf8'))
    const pathParts = filePath.substring(0, filePath.length - 5).split(sep)

    const _id = content.id || crypto.createHash('md5').update(JSON.stringify(content)).digest('hex')
    const _type = pathParts[0]
    const _slug = pathParts[pathParts.length - 1]

    const imageArray = _type === 'product' ? getImageArray(content) : undefined
    const pictures = imageArray ? getPictures(imageArray) : undefined
    const image = _type === 'product' && imageArray.length ? imageArray[0] : content.image || '/ab-image-not-found.png'

    if (
      _type === 'product' &&
      (content.category === 'Garnalen' || content.subcategory === 'Caridina' || content.subcategory === 'Neocaridina')
    ) {
      content.description += "\n\nKijk voor alle garnalen en toebehoren onze website <a href=\"https://degarnalenwinkel.nl\" target=\"_blank\">degarnalenwinkel.nl</a>"
    }

    data[_id] = { ...content, _id, _type, _slug, imageArray, pictures, image }
    byType[_type] = byType[_type] || []
    byType[_type].push(_id)
  })

  const routeBase = '/producten'
  const categoryDisplayAll = 'Alle producten'
  const routes = [{
    title: 'Producten',
    path: routeBase
  }]
  const tree = byType.product_category.reduce((acc, categoryId) => {
    const categoryObj = data[categoryId]

    if (categoryObj.active) {
      const productsByCategoryEntry = {}
      const { _id } = categoryObj

      const subcategories = byType.product_subcategory.reduce((acc, subcategoryId) => {
        const subcategoryObj = data[subcategoryId]
        if (subcategoryObj.active && subcategoryObj.category === categoryObj.title) {
          const { _id } = subcategoryObj
          const products = byType.product.reduce((acc, productId) => {
            const productObj = data[productId]
            if (productObj.active && productObj.subcategory === subcategoryObj.title) {
              routes.push({
                title: productObj.title,
                path: [routeBase, categoryObj._slug, subcategoryObj._slug, productObj._slug].join('/')
              })

              acc[productObj._slug] = {
                _id: productId,
                data: null
              }

              const productFullPath = [subcategoryObj._slug, productObj._slug].join('/')
              productsByCategoryEntry[productFullPath] = acc[productObj._slug]
            }
            return acc
          }, {})

          const hasProducts = !!Object.entries(products).length

          if (hasProducts) {
            routes.push({
              title: subcategoryObj.title,
              path: [routeBase, categoryObj._slug, subcategoryObj._slug].join('/')
            })

            acc[subcategoryObj._slug] = {
              _id,
              products,
              data: null
            }
          }
        }

        return acc
      }, {})

      const products = byType.product.reduce((acc, productId) => {
        const productObj = data[productId]
        if (productObj.active && productObj.category === categoryObj.title) {
          routes.push({
            title: productObj.title,
            path: [routeBase, categoryObj._slug, productObj._slug].join('/')
          })

          const { _id } = productObj
          acc[productObj._slug] = {
            _id,
            data: null
          }

          productsByCategoryEntry[productObj._slug] = acc[productObj._slug]
        }

        return acc
      }, {})

      const hasSubcategories = !!Object.entries(subcategories).length
      const hasProducts = !!Object.entries(products).length

      if (hasSubcategories || hasProducts) {
        routes.push({
          title: categoryObj.title,
          path: [routeBase, categoryObj._slug].join('/')
        })
        routes.push({
          title: categoryDisplayAll,
          path: [routeBase, categoryObj._slug, 'alles'].join('/')
        })

        acc[categoryObj._slug] = {
          _id,
          data: null,
          products,
          subcategories
        }

        if (hasSubcategories) {
          productsByCategory[categoryObj._slug] = productsByCategoryEntry
        }
      }
    }

    return acc
  }, {})

  return {
    data,
    tree,
    routes,
    byType,
    productsByCategory,
    lastUpdate: (new Date()).getTime()
  }
}

module.exports = { getFormattedData }
