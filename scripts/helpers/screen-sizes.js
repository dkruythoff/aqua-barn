const { theme: { screens } } = require('tailwindcss/stubs/defaultConfig.stub')

module.exports = {
  screens
}
