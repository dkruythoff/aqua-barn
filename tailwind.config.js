module.exports = {
  prefix: "",
  important: false,
  separator: ":",
  theme: {
    extend: {
      colors: {
        'ab-yellow': {
          100: '#FDFBF5',
          200: '#FBF4E6',
          300: '#F8EED7',
          400: '#F3E1B8',
          500: '#EED49A',
          600: '#D6BF8B',
          700: '#8F7F5C',
          800: '#6B5F45',
          900: '#47402E'
        }
      },
      height: {
        72: "18rem",
        96: "24rem",
        128: "32rem"
      },
      width: {
        72: "18rem",
        96: "24rem",
        128: "32rem"
      }
    }
  },
  variants: {},
  plugins: [
    require("tailwindcss-grid")({
      grids: [2, 3, 4, 5, 6, 8, 10, 12],
      gaps: {
        0: "0",
        1: "0.25rem",
        2: "0.5rem",
        3: "0.75rem",
        4: "1rem",
        8: "2rem"
      },
      autoMinWidths: {
        16: "4rem",
        24: "6rem",
        "300px": "300px"
      },
      variants: ["responsive"]
    })
  ]
};
